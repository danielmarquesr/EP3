Rails.application.routes.draw do

  devise_for :admins
  get 'carrinho' => 'carrinho#index'
  get 'carrinho/limpar' => 'carrinho#limpar'
  get 'carrinho/:id' => 'carrinho#add'


  resources :bebidas
  root to: 'pagina#home'

  get 'pagina/home'

  get 'pagina/sobre'

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
