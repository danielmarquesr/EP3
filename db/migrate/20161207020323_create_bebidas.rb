class CreateBebidas < ActiveRecord::Migration[5.0]
  def change
    create_table :bebidas do |t|
      t.string :nome
      t.float :preco
      t.integer :litro
      t.string :categoria
      t.string :pais
      t.text :descricao

      t.timestamps
    end
  end
end
