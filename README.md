# README

Daniel Marques Rangel - 15/008228

Filipe Miranda de Macedo 15/0125518

Para executar a aplicação siga os seguintes passos:

1- Instale o software "imagemagick" (sudo apt-get install imagemagick);
2- Clone o repositório para sua máquina;
3- No terminal acessa a pasta da aplicação;
4- Instale as gems usadas com o comando "bundle install"
5- Execute o comando 'rake db:migrate' para atualizar o banco de dados (sqlit3);
4- Execute o comando "rails s" para levantar o servidor local;
5- Abra seu navegador e digite na barra de endereço "localhost:3000";
6- Navegue pela aplicação através da barra de navegação.

Obs: gems usadas: 'devise', 'paperclip' e 'twitter-bootstrap-rails', para mais informações das gems acesse o arquivo "Gemfile" dentro da pasta do projeto.
